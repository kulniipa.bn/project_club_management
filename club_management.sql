-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 23, 2018 at 07:36 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `club_management`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_check_student`
--

CREATE TABLE `tbl_check_student` (
  `id` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `create_by` varchar(255) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(255) DEFAULT NULL,
  `update_date` timestamp NULL DEFAULT NULL,
  `delete_status` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_clubs`
--

CREATE TABLE `tbl_clubs` (
  `cid` int(11) NOT NULL,
  `clubname` varchar(255) NOT NULL,
  `day` varchar(50) NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `create_by` varchar(255) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(255) DEFAULT NULL,
  `update_date` timestamp NULL DEFAULT NULL,
  `delete_status` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_clubs`
--

INSERT INTO `tbl_clubs` (`cid`, `clubname`, `day`, `start_time`, `end_time`, `create_by`, `create_date`, `update_by`, `update_date`, `delete_status`) VALUES
(1, 'atestclub', 'monday', '11:00:00', '12:00:00', '4', '2018-10-20 18:57:59', NULL, NULL, b'0'),
(2, 'testclub', 'tuesday', '02:00:00', '00:00:00', '4', '2018-10-20 18:58:05', NULL, NULL, b'0'),
(3, 'pingpong', 'wednesday', '02:00:00', '11:00:00', '4', '2018-10-20 18:40:34', NULL, NULL, b'1'),
(4, 'tennis', 'monday', '01:00:00', '01:00:00', '4', '2018-10-20 18:40:36', NULL, NULL, b'1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_club_reltionship`
--

CREATE TABLE `tbl_club_reltionship` (
  `id` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `tid` int(11) NOT NULL,
  `taid` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `create_by` varchar(255) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(255) DEFAULT NULL,
  `update_date` timestamp NULL DEFAULT NULL,
  `delete_status` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_club_reltionship`
--

INSERT INTO `tbl_club_reltionship` (`id`, `cid`, `sid`, `tid`, `taid`, `pid`, `create_by`, `create_date`, `update_by`, `update_date`, `delete_status`) VALUES
(1, 1, 1, 4, 3, 1, '4', '2018-10-20 18:24:04', NULL, NULL, b'1'),
(2, 1, 2, 3, 3, 1, '4', '2018-10-20 17:28:36', NULL, NULL, b'1'),
(3, 3, 2, 3, 3, 1, '4', '2018-10-20 17:28:47', NULL, NULL, b'1'),
(4, 3, 2, 3, 3, 1, '4', '2018-10-20 17:28:47', NULL, NULL, b'1'),
(5, 1, 3, 2, 2, 2, '4', '2018-10-20 20:09:55', NULL, NULL, b'0'),
(6, 1, 4, 2, 4, 2, '4', '2018-10-20 20:17:34', NULL, NULL, b'0'),
(7, 1, 3, 2, 2, 2, '4', '2018-10-20 20:22:16', NULL, NULL, b'0'),
(8, 1, 3, 3, 2, 2, '4', '2018-10-20 20:22:16', NULL, NULL, b'0'),
(9, 1, 3, 2, 7, 2, '4', '2018-10-21 04:26:11', NULL, NULL, b'0'),
(10, 1, 5, 2, 2, 2, '4', '2018-10-21 04:27:04', NULL, NULL, b'0'),
(11, 2, 3, 2, 8, 2, '4', '2018-10-21 04:27:49', NULL, NULL, b'0'),
(12, 2, 3, 3, 8, 2, '4', '2018-10-21 04:27:49', NULL, NULL, b'0'),
(13, 1, 3, 2, 2, 2, '4', '2018-10-21 04:29:27', NULL, NULL, b'0'),
(14, 1, 3, 2, 3, 2, '4', '2018-10-21 04:29:27', NULL, NULL, b'0'),
(15, 1, 3, 2, 8, 1, '4', '2018-10-21 06:03:13', NULL, NULL, b'0'),
(16, 1, 3, 2, 7, 1, '4', '2018-10-21 06:03:41', NULL, NULL, b'0'),
(17, 1, 3, 2, 11, 1, '4', '2018-10-21 06:03:41', NULL, NULL, b'0'),
(18, 0, 0, 2, 3, 1, '4', '2018-10-21 06:04:04', NULL, NULL, b'0'),
(19, 0, 0, 2, 3, 2, '4', '2018-10-21 06:04:04', NULL, NULL, b'0'),
(20, 0, 0, 2, 2, 1, '4', '2018-10-21 06:04:04', NULL, NULL, b'0'),
(21, 0, 0, 2, 2, 2, '4', '2018-10-21 06:04:04', NULL, NULL, b'0'),
(22, 0, 0, 2, 2, 1, '4', '2018-10-21 06:04:29', NULL, NULL, b'0'),
(23, 0, 0, 2, 2, 2, '4', '2018-10-21 06:04:29', NULL, NULL, b'0'),
(24, 0, 0, 2, 8, 1, '4', '2018-10-21 06:04:29', NULL, NULL, b'0'),
(25, 0, 0, 2, 8, 2, '4', '2018-10-21 06:04:29', NULL, NULL, b'0'),
(26, 0, 0, 3, 2, 1, '4', '2018-10-21 06:04:29', NULL, NULL, b'0'),
(27, 0, 0, 3, 2, 2, '4', '2018-10-21 06:04:29', NULL, NULL, b'0'),
(28, 0, 0, 3, 8, 1, '4', '2018-10-21 06:04:29', NULL, NULL, b'0'),
(29, 0, 0, 3, 8, 2, '4', '2018-10-21 06:04:29', NULL, NULL, b'0'),
(30, 0, 0, 2, 4, 1, '4', '2018-10-21 06:05:09', NULL, NULL, b'0'),
(31, 0, 0, 3, 4, 1, '4', '2018-10-21 06:05:09', NULL, NULL, b'0'),
(32, 1, 5, 2, 4, 1, '4', '2018-10-21 06:05:26', NULL, NULL, b'0'),
(33, 1, 5, 2, 4, 2, '4', '2018-10-21 06:05:26', NULL, NULL, b'0'),
(34, 1, 5, 3, 4, 2, '4', '2018-10-21 06:05:26', NULL, NULL, b'0'),
(35, 1, 5, 3, 4, 1, '4', '2018-10-21 06:05:26', NULL, NULL, b'0'),
(36, 1, 6, 2, 7, 1, '4', '2018-10-21 06:05:41', NULL, NULL, b'0'),
(37, 1, 6, 2, 11, 1, '4', '2018-10-21 06:05:41', NULL, NULL, b'0'),
(38, 1, 6, 3, 7, 1, '4', '2018-10-21 06:05:41', NULL, NULL, b'0'),
(39, 1, 6, 3, 11, 1, '4', '2018-10-21 06:05:41', NULL, NULL, b'0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_partners`
--

CREATE TABLE `tbl_partners` (
  `pid` int(11) NOT NULL,
  `partner_name` varchar(255) NOT NULL,
  `aptitude` varchar(255) NOT NULL,
  `contact_person` varchar(255) NOT NULL,
  `tel` int(10) NOT NULL,
  `email` varchar(255) NOT NULL,
  `create_by` varchar(255) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(255) DEFAULT NULL,
  `update_date` timestamp NULL DEFAULT NULL,
  `delete_status` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_partners`
--

INSERT INTO `tbl_partners` (`pid`, `partner_name`, `aptitude`, `contact_person`, `tel`, `email`, `create_by`, `create_date`, `update_by`, `update_date`, `delete_status`) VALUES
(1, 'partner', 'testtsg', 'joy', 927826594, 'hrr@dd.d', '4', '2018-10-21 05:07:01', NULL, NULL, b'0'),
(2, 'partner', 'c#', 'gffee', 35353535, 'et@fsf', '4', '2018-10-14 17:41:44', NULL, NULL, b'0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_students`
--

CREATE TABLE `tbl_students` (
  `sid` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `year` int(11) NOT NULL,
  `create_by` int(255) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(255) DEFAULT NULL,
  `update_date` timestamp NULL DEFAULT NULL,
  `delete_status` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_students`
--

INSERT INTO `tbl_students` (`sid`, `firstname`, `lastname`, `year`, `create_by`, `create_date`, `update_by`, `update_date`, `delete_status`) VALUES
(1, 'test', 'fmnlmfs', 1, 4, '2018-10-20 18:57:34', NULL, NULL, b'1'),
(2, 'kaa', 'aaa', 2, 4, '2018-10-14 16:02:55', NULL, NULL, b'0'),
(3, 'pattama', 'laokordeee', 2, 4, '2018-10-14 17:05:08', NULL, NULL, b'0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tas`
--

CREATE TABLE `tbl_tas` (
  `taid` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `tel` int(10) NOT NULL,
  `email` varchar(255) NOT NULL,
  `create_by` varchar(255) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(255) DEFAULT NULL,
  `update_date` timestamp NULL DEFAULT NULL,
  `delete_status` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_tas`
--

INSERT INTO `tbl_tas` (`taid`, `firstname`, `lastname`, `tel`, `email`, `create_by`, `create_date`, `update_by`, `update_date`, `delete_status`) VALUES
(1, '1', '2', 3, '4', '4', '2018-10-14 09:06:38', NULL, NULL, b'1'),
(2, 'joy', 'pattama', 927826594, 'test@s.a', '4', '2018-10-13 21:10:53', NULL, NULL, b'0'),
(3, 'ee', 'oo', 557, 'hjy@fe.e', '4', '2018-10-14 08:52:01', NULL, NULL, b'0'),
(4, 'ttyt', 'tyt', 5565, 'gr@rfr.e', '4', '2018-10-14 08:57:16', NULL, NULL, b'0'),
(5, '[oo', '[o[oo', 5255, 'we@d.d', '4', '2018-10-20 18:57:24', NULL, NULL, b'1'),
(6, 'dsds', 'dada', 54545, 'ada@ddd.d', '4', '2018-10-14 08:59:48', NULL, NULL, b'0'),
(7, 'ada', 'yt', 0, 'ada2jdjd.cs', '4', '2018-10-14 09:00:53', NULL, NULL, b'0'),
(8, 'saa', 'sa', 225, 'jhdj@d.d', '4', '2018-10-14 09:03:50', NULL, NULL, b'0'),
(9, 'uu', 'ui', 5757, 'dgr@d.d', '4', '2018-10-14 09:05:11', NULL, NULL, b'0'),
(10, 'ddd', 'ddd', 57577, 'fs@s.s', '4', '2018-10-14 09:05:37', NULL, NULL, b'0'),
(11, 'd', 'd', 22, 'd@s.s', '4', '2018-10-14 17:04:26', NULL, NULL, b'0'),
(12, 'ffdfd', 'dfdf', 5355, 'fd@d.d', '4', '2018-10-20 19:08:27', NULL, NULL, b'1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_teachers`
--

CREATE TABLE `tbl_teachers` (
  `tid` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `tel` int(10) NOT NULL,
  `email` varchar(255) NOT NULL,
  `create_by` varchar(255) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(255) DEFAULT NULL,
  `update_date` timestamp NULL DEFAULT NULL,
  `delete_status` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_teachers`
--

INSERT INTO `tbl_teachers` (`tid`, `firstname`, `lastname`, `tel`, `email`, `create_by`, `create_date`, `update_by`, `update_date`, `delete_status`) VALUES
(3, '5', '2', 3, '4', '4', '2018-10-12 05:58:29', NULL, NULL, b'0'),
(4, 'weew', 'wew', 116, 'wwr@d.d', '4', '2018-10-14 09:08:44', NULL, NULL, b'0'),
(5, 'd', 'd', 0, 'd@s.s', '4', '2018-10-14 17:04:42', NULL, NULL, b'0'),
(6, 'g', 'g', 512, 'gg@r.r', '4', '2018-10-14 17:07:11', NULL, NULL, b'0'),
(7, 'ee', 'ee', 24244, 'h@fs.v', '4', '2018-10-21 11:45:55', NULL, NULL, b'0'),
(8, 'd', 'g', 0, 'gg@r.r', '4', '2018-10-21 11:55:11', NULL, NULL, b'0'),
(9, 'd', 'aaaa', 455668, 'fd@d.d', '4', '2018-10-21 14:50:23', NULL, NULL, b'0'),
(10, '222', 'lll', 2155665, 'kmlk@h.s', '4', '2018-10-21 14:52:13', NULL, NULL, b'0'),
(11, 'ss', 'ss', 47545, 'ss@e.e', '4', '2018-10-21 15:03:11', NULL, NULL, b'0'),
(12, 'ss', 'aa', 3221, 'sa@s.s', '4', '2018-10-21 15:05:20', NULL, NULL, b'0'),
(13, 'sad', 'ad', 5451152, 'a@hjd.d', '4', '2018-10-21 15:07:40', NULL, NULL, b'0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `uid` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `create_by` varchar(255) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(255) DEFAULT NULL,
  `update_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `delete_status` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`uid`, `username`, `password`, `create_by`, `create_date`, `update_by`, `update_date`, `delete_status`) VALUES
(4, 'test', 'c4ca4238a0b923820dcc509a6f75849b', 'test', '2018-09-29 14:29:22', NULL, '0000-00-00 00:00:00', b'0'),
(5, 'yyy', 'c4ca4238a0b923820dcc509a6f75849b', 'yyy', '2018-10-10 16:12:02', NULL, '0000-00-00 00:00:00', b'0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_check_student`
--
ALTER TABLE `tbl_check_student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_clubs`
--
ALTER TABLE `tbl_clubs`
  ADD PRIMARY KEY (`cid`);

--
-- Indexes for table `tbl_club_reltionship`
--
ALTER TABLE `tbl_club_reltionship`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_partners`
--
ALTER TABLE `tbl_partners`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `tbl_students`
--
ALTER TABLE `tbl_students`
  ADD PRIMARY KEY (`sid`);

--
-- Indexes for table `tbl_tas`
--
ALTER TABLE `tbl_tas`
  ADD PRIMARY KEY (`taid`);

--
-- Indexes for table `tbl_teachers`
--
ALTER TABLE `tbl_teachers`
  ADD PRIMARY KEY (`tid`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`uid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_check_student`
--
ALTER TABLE `tbl_check_student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_clubs`
--
ALTER TABLE `tbl_clubs`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_club_reltionship`
--
ALTER TABLE `tbl_club_reltionship`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `tbl_partners`
--
ALTER TABLE `tbl_partners`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_students`
--
ALTER TABLE `tbl_students`
  MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_tas`
--
ALTER TABLE `tbl_tas`
  MODIFY `taid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tbl_teachers`
--
ALTER TABLE `tbl_teachers`
  MODIFY `tid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
