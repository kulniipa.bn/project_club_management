const { club } = require("../../repository");
let data = {};

const getHandler = async ctx => {
  await ctx.render("club");
};

const getListClub = async ctx => {
  if (ctx.params.id == "1" || ctx.params.id == "7" || !ctx.params.id) {
    const { checktype } = ctx.request.body;
    const id = ctx.params.id ? ctx.params.id : "1";
    let type = checktype ? checktype : "TA";

    if (ctx.params.id == "7") {
      type = "Teacher";
    }
    const clubObject = await club.listData(id, type);
    data.clubs = clubObject;
    data.user_types = { user_type: ctx.params.id };
  } else if (
    ctx.params.id != "1" &&
    ctx.params.id != "3" &&
    ctx.params.id != "6" &&
    ctx.params.id
  ) {
    const clubObject = await club.listData(ctx.params.id);
    data.clubs = clubObject;
    data.user_types = { user_type: ctx.params.id };
  } else if (ctx.params.id == "3") {
    const clubObject = await club.listData(ctx.params.id);
    const clubName = await club.listClub();
    const StudentObject = await club.listStudent();
    data.clubs = clubObject;
    data.clubNames = clubName;
    data.students = StudentObject;
    data.user_types = { user_type: ctx.params.id };
  } else if (ctx.params.id == "6") {
    const clubObject = await club.listData(ctx.params.id);
    const clubName = await club.listClub();
    const TeacherObject = await club.listTeacher();
    const StudentObject = await club.listStudent();
    const TaObject = await club.listTa();
    const PartnerObject = await club.lisPartner();
    data.clubs = clubObject;
    data.clubNames = clubName;
    data.teachers = TeacherObject;
    data.students = StudentObject;
    data.tas = TaObject;
    data.partners = PartnerObject;
    data.user_types = { user_type: ctx.params.id };
  }
  await ctx.render("/club", data);
};

const postEditHandler = async ctx => {
  const { 
    activities,
    select_ta_teacher,
    checktype
  } = ctx.request.body;
  console.log(select_ta_teacher);
  if (activities == "1") {
    const data = { 
      ta_firstname,
      ta_lastname,
      ta_telephone,
      ta_email
    } = ctx.request.body;
    const result = await club.updateData(ctx.params.id, activities,data);  
  } else if (activities == "2") {
    const data = { 
      student_firstname,
      student_lastname,
      student_year
    } = ctx.request.body;
    const result = await club.updateData(ctx.params.id, activities,data);    
  } else if (activities == "3") {
    // check student
    const data = { 
      hidden_id,
      ddl_check_clubname,
      ddl_check_student
    } = ctx.request.body;
    console.log(data);
  } else if (activities == "4") {
    const data = { 
      clubname,
      club_day,
      start_time,
      end_time 
    } = ctx.request.body;
    const result = await club.updateData(ctx.params.id, activities,data);    
  } else if (activities == "5") {
    const data = { 
      partner_name,
      partner_tel,
      partner_email,
      personal_contact 
    } = ctx.request.body;
    const result = await club.updateData(ctx.params.id, activities,data);
  } else if (activities == "6") {
    //club relationship
  } else if (activities == "7"){
    const data = { 
      ta_firstname,
      ta_lastname,
      ta_telephone,
      ta_email
    } = ctx.request.body;
    const result = await club.updateData(ctx.params.id, activities,data);      
  }
  const clubObject = await club.listData(activities);
  data.clubs = clubObject;
  /*if (select_ta_teacher == "Teacher") {
    await ctx.redirect("/club/7", data);
  } else {
    await ctx.redirect("/club/" + activities);
  }*/

  /*console.log('ctx.params.id : ',ctx.params.id);
  if (ctx.params.id == 7) {
    await ctx.redirect("/club/7", data);
  } else {
    await ctx.redirect("/club/" + activities);
  }*/

  if (activities == "1" && checktype == "Teacher") {
    await ctx.redirect("/club/7", data);
  } else {
    await ctx.redirect("/club/" + activities);
  }  
}

const postHandler = async ctx => {
  const {
    activities,
    checktype,
    ta_firstname,
    ta_lastname,
    ta_telephone,
    ta_email,
    student_firstname,
    student_lastname,
    student_year,
    clubname,
    club_day,
    start_time,
    end_time,
    partner_name,
    aptitude,
    personal_contact,
    partner_tel,
    partner_email,
    ddl_clubname,
    ddl_teacher,
    ddl_student,
    ddl_ta,
    ddl_partner,
    ddl_check_clubname,
    ddl_check_student
  } = ctx.request.body;

  const create_by = ctx.session.userId;

  if (activities == "1") {
    const tid = await club.registerTAOrTeacher(
      ta_firstname,
      ta_lastname,
      ta_telephone,
      ta_email,
      create_by,
      checktype
    );
  } else if (activities == "2") {
    const sid = await club.registerStudent(
      student_firstname,
      student_lastname,
      student_year,
      create_by
    );
  } else if (activities == "3") {
    const id = await club.registerCheckStudent(
      ddl_check_clubname,
      ddl_check_student,
      create_by
    );
  } else if (activities == "4") {
    const cid = await club.registerClub(
      clubname,
      club_day,
      start_time,
      end_time,
      create_by
    );
  } else if (activities == "5") {
    const pid = await club.registerPartner(
      partner_name,
      aptitude,
      personal_contact,
      partner_tel,
      partner_email,
      create_by
    );
  } else if (activities == "6") {
    const rid = await club.registerRelationship(
      ddl_clubname,
      ddl_teacher,
      ddl_student,
      ddl_ta,
      ddl_partner,
      create_by
    );
  }
  if (activities == "1" && checktype == "Teacher") {
    await ctx.redirect("7");
  } else {
    await ctx.redirect(activities);
  }
};

const postDeleteHandler = async ctx => {
  const { activities, checktype } = ctx.request.body;
  console.log('checktype delete : ',checktype)
  if (activities == "1") {
    const result = await club.deleteData(ctx.params.id, activities, checktype);
  } else if (activities != "1") {
    const result = await club.deleteData(ctx.params.id, activities, "");
  }
  if (activities == "1" && checktype == "Teacher") {
    await ctx.redirect("/club/7", data);
  } else {
    await ctx.redirect("/club/" + activities);
  }
};

module.exports = {
  getHandler,
  postHandler,
  getListClub,
  postDeleteHandler,
  postEditHandler
};
