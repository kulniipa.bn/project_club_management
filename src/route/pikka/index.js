const Router = require("koa-router");
const club = require("./club");

const router = new Router();

const checkAuth = async (ctx, next) => {
  if (!ctx.session || !ctx.session.userId) {
    ctx.redirect("../signin");
  }
  await next();
};

router.get("/", checkAuth, club.getListClub);
router.get("/club/:id", checkAuth, club.getListClub);
router.post("/club/:id", checkAuth, club.postHandler);
router.post("/club/delete/:id", checkAuth, club.postDeleteHandler);
router.post("/club/update/:id", checkAuth, club.postEditHandler);

module.exports = router.routes();
