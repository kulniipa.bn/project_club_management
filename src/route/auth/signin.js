const { user } = require("../../repository");
const { club } = require("../../repository");
const md5 = require("md5");

let data = {};

const getHandler = async ctx => {
  await ctx.render("signin");
};

const postHandler = async ctx => {
  const users = await user.login(ctx.request.body.username);
  const same = await md5(ctx.request.body.password, users.password);
  if (!same) {
    ctx.session.flash = { error: "username or password is wrong" };
    return ctx.redirect("signin");
  }

  ctx.session.userId = users.uid;
  const clubObject = await club.listData(1);
  data.clubs = clubObject;
  await ctx.redirect("/club/1", data);
};

module.exports = {
  getHandler,
  postHandler
};
