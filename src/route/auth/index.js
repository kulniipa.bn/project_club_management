const Router = require("koa-router");
const signIn = require("./signin");
const signUp = require("./signup");

const router = new Router();

const checkAuth = async (ctx, next) => {
  if (ctx.session && ctx.session.userId) {
    ctx.redirect("club");
  }
  await next();
};

async function signinGetHandler(ctx) {
  const data = {
    flash: ctx.flash
  };
  await ctx.render("signin", data);
}

async function signinPostHandler(ctx) {
  const data = {
    flash: ctx.flash
  };
  await ctx.render("signup", data);
}

router.get("/signin", checkAuth, signinGetHandler, signIn.getHandler);
router.post("/signin", checkAuth, signIn.postHandler);
router.get("/signup", checkAuth, signinPostHandler, signUp.getHandler);
router.post("/signup", signUp.postHandler);

module.exports = router.routes();
