const { user } = require("../../repository");
const md5 = require("md5");
const flash = require("flash");

const getHandler = async ctx => {
  await ctx.render("signup");
};

const postHandler = async ctx => {
  const { username, password, confirmPassword } = ctx.request.body;

  if (password !== confirmPassword) {
    //console.log("Password does not match!!");
    ctx.session.flash = { error: "Password does not match!!" };
    return ctx.redirect("signup");
  } else {
    const hashedPassword = await md5(password);
    const userId = await user.register(username, hashedPassword);
  }

  //ctx.redirect("signin");
  ctx.redirect("signin");
};

module.exports = {
  getHandler,
  postHandler
};
