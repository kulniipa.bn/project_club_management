const pool = require("../db");

class User {
  constructor(row) {
    this.uid = row.uid;
    this.username = row.username;
    this.password = row.password;
  }
}

async function register(username, password) {
  const result = await pool.query(
    `
		insert into tbl_users
			(username, password,create_by)
		values
			(?, ?, ?)
	`,
    [username, password, username]
  );
  return result[0].insertId;
}

const login = async username => {
  const [rows] = await pool.query(
    `select uid,username,password 
	from tbl_users where username=? and delete_status=0`,
    [username]
  );
  return new User(rows[0]);
};

async function getPasswordByUsername(username) {
  const result = await pool.query(
    `
		select
			password
		from tbl_users
		where username = ?
	`,
    [username]
  );
  return result[0].password;
}

module.exports = {
  register,
  login,
  getPasswordByUsername
};
